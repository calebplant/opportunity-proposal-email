@isTest
public with sharing class QuoteEmailService_Test {
    
    @TestSetup
    static void makeData(){
        Id standardPb = Test.getStandardPricebookId();
        // Test.setCreatedDate(a.Id, DateTime.newInstance(2012,12,12));

        List<Product2> prods = new List<Product2>();
        prods.add(new Product2(Name='prod1', IsActive=true));
        prods.add(new Product2(Name='prod2', IsActive=true));
        prods.add(new Product2(Name='prod3', IsActive=true));
        insert prods;
        List<PricebookEntry> pbes = new List<PricebookEntry>();
        pbes.add(new PricebookEntry(Product2Id=prods[0].id, Pricebook2ID=standardPb, UnitPrice=5, isActive=true));
        pbes.add(new PricebookEntry(Product2Id=prods[1].id, Pricebook2ID=standardPb, UnitPrice=10, isActive=true));
        pbes.add(new PricebookEntry(Product2Id=prods[2].id, Pricebook2ID=standardPb, UnitPrice=15, isActive=true));
        insert pbes;
        Account acc = new Account(Name='testAcc');
        insert acc;
        Contact con = new Contact(Lastname='testCon',Email='abc@gmail.com',Phone='1234567890', AccountId=acc.Id);
        insert con;
        Opportunity opp = new Opportunity(Name='testOpp', AccountId=acc.id, Pricebook2Id=standardPb,
                                        CloseDate=System.today().addDays(10),StageName='Prospecting');
        insert opp;
        Quote qt = new Quote(Name='test quote',OpportunityId=opp.id,Pricebook2Id = standardPb, ContactId=con.id,
                            ExpirationDate=System.today().addDays(10));
        insert qt;
        Quote newQt = new Quote(Name='test quote newest',OpportunityId=opp.id,Pricebook2Id = standardPb, ContactId=con.id,
                            ExpirationDate=System.today().addDays(10));
        insert newQt;
        Test.setCreatedDate(newQt.Id, System.today().addDays(1));
        List<QuoteLineItem> qtLineItems = new List<QuoteLineItem>();
        qtLineItems.add(new QuoteLineItem(QuoteId = qt.Id, Quantity = 10, 
                              PricebookEntryId = pbes[0].Id, UnitPrice=pbes[0].UnitPrice));
        qtLineItems.add(new QuoteLineItem(QuoteId = qt.Id, Quantity = 10, 
                              PricebookEntryId = pbes[1].Id, UnitPrice=pbes[1].UnitPrice));
        qtLineItems.add(new QuoteLineItem(QuoteId = qt.Id, Quantity = 10, 
                              PricebookEntryId = pbes[2].Id, UnitPrice=pbes[2].UnitPrice));
        insert qtLineItems;
    }

    @isTest
    static void doesSendQuoteEmailCauseError()
    {
        System.debug('testerfunc');
        Opportunity opp = [SELECT Id, StageName FROM Opportunity WHERE Name = 'testOpp'];
        opp.StageName = 'Proposal/Price Quote';

        Test.startTest();
        update opp;
        Test.stopTest();
    }
}
