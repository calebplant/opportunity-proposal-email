public with sharing class QuoteEmailService {

    private static Id emailTemplateId;

    @InvocableMethod(label='Send Quote Proposal Email')
    public static void sendQuoteEmail(List<QuoteInputs> inputs)
    {
        System.debug('START sendQuoteEmail');
        List<Messaging.SingleEmailMessage> outboundEmails = new List<Messaging.SingleEmailMessage>();
        emailTemplateId = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Opportunity_Proposal_Template' LIMIT 1].Id;

        for(QuoteInputs eachInput : inputs) {
            System.debug(eachInput.passedQuote);
            outboundEmails.add(generateQuoteEmail(eachInput.passedQuote));
        }
        // Messaging.SingleEmailMessage email = Messaging.renderStoredEmailTemplate(templateId, whoId, whatId);
        System.debug('Sending Emails...');
        System.debug(outboundEmails);
        Messaging.sendEmail(outboundEmails);
        System.debug('Email sent');
        // try{
        //     Messaging.sendEmail(outboundEmails);
        //     System.debug('Email sent');
        // } catch(EmailException e) {
        //     System.debug('ERROR SENDING EMAIL:');
        //     notifyDevelopersOf(e, 'sendQuoteEmail');
        // }
        // }
        
    }
    
    public with sharing class QuoteInputs {
        @InvocableVariable(label='Quote' required=true)
        public Quote passedQuote;
    }

    private static Messaging.SingleEmailMessage generateQuoteEmail(Quote qt)
    {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTargetObjectId(qt.ContactId);
        email.setWhatId(qt.Id);
        email.setTemplateId(emailTemplateId);
        email.setUseSignature(false);
        email.setTreatTargetObjectAsRecipient(true);
        email.setSaveAsActivity(false);
        return email;
    }

    // private static void notifyDevelopersOf(Exception e, String locationName) {
    //     String orgId = UserInfo.getOrganizationId();
    //     String orgName = UserInfo.getOrganizationName();
    //     String user = UserInfo.getUserName();
    //     String message = e.getMessage();
    //     String stacktrace = e.getStackTraceString();
    //     String exType = '' + e.getTypeName();
    //     String line = '' + e.getLineNumber();
    //     String theTime = '' + System.now();
    
    //     String subject = String.format('Exception thrown at {0} by user {1} in org {2} (Id: {3})', new List<String>{ locationName, user, orgName, orgId });
    //     String body = String.format('Time: {0}\nMessage: {1}\nStacktrace: {2}\nLine: {3}', new List<String>{ theTime, message, stacktrace, line });
    
    //     Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    //     String[] toAddresses = new String[] {'calebplant@aggienetwork.com'}; // Placeholder
    //     mail.setToAddresses(toAddresses);
    //     mail.setSubject(subject);
    //     mail.setUseSignature(false);
    //     mail.setPlainTextBody(body);
    
    //     Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    // }
}
