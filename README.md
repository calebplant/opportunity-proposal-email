# Opportunity Proposal Email

## Overview

A simple configuration that emails the most recent Quote (converted to tabular form in the body) to an Opportunity's contact when the stage is changed to Proposal.

## Demo (~40 sec)

[Here's a link to a demo video for the component](https://www.youtube.com/watch?v=YCXBjqi7cXg)

## Some Screenshots

### Example Email

![Example Email](media/example-email.png)

### Before Update Flow

![Before Update Flow](media/before-update-flow.png)

### After Update Flow

![After Update Flow](media/after-update-flow.png)

## Functional Overview

The configuration utilizes before and after update record-triggered flows on Opportunity to determine if an email should be sent. If so, the after-update flow calls an Apex function that prepares and sends the email. The email is generated using a Visualforce Email Template.

### Files

* **QuoteEmailService.cls** - Apex class invoced by the after-update flow to send the email. Grabs the most recent quote and then sets up an email to send utilizing a VF email template.
* **Opportunity_Proposal_Tempalte.email** - VF email template used by QuoteEmailService to generate the body of the sent email.